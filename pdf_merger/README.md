# PDF merger

There are tons of online PDF mergers on the web, but sometimes you don't want to share your confidental documents with a random page. So here's an offline PDF merger for free.

### Prerequisites

```
Python 2.x
PyPDF2
docopt
```

### Installing

```
pip install PyPDF2
pip install docopt
```

## Acknowledgments

* https://gist.github.com/PurpleBooth for readme.md template
* https://stackoverflow.com/questions/3444645/merge-pdf-files code snippet