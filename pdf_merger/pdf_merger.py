"""PDF Mergererer

Usage:
  pdf_merger.py [--path <folder>]
  pdf_merger.py (-h | --help)

Options:
  -h --help         Show this screen.
  --path <folder>   Input folder [default: .\]

"""

import os
from PyPDF2 import PdfFileMerger
from docopt import docopt

def main(directory):
	'''
	All the PDFs are going to be merged in the given folder in alphabetical order.
	'''

	pdf_list = [c_file for c_file in os.listdir(directory) if c_file.endswith('.pdf')]

	merger = PdfFileMerger()

	for pdf in pdf_list:
		merger.append(open(directory + pdf, 'rb'))

	with open(directory + 'merged.pdf', 'wb') as fout:
		merger.write(fout)



if __name__== "__main__":
	arguments = docopt(__doc__)
	directory = arguments['--path']
	main(directory)